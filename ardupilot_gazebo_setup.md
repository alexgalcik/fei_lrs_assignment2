This tutorial is aimed to install all dependencies for LRS course on Linux systems. The following guide was only tested on Ubuntu 18.04 and compatibility with other Ubuntu releases is not guaranteed.

You may find this useful: [Basic Linux Commands for Beginners](https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners)

_Pro tip: for pasting commands into your terminal, use Shift+Insert instead of Ctrl+V._

## 0. Fresh ubuntu 
```
sudo apt-get install git
```

## 1. Ardupilot (Copter)
You need to build ardupilot to be able to simulate [Ardupilot behaviour with SITL](https://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html)
```bash
cd ~
git clone -b Copter-4.2.3 https://github.com/ArduPilot/ardupilot.git
cd ardupilot
git submodule update --init --recursive
Tools/environment_install/install-prereqs-ubuntu.sh -y
. ~/.profile
./waf configure --board sitl 
./waf copter
```

We have to check TCP and UDP access:
```bash
sim_vehicle.py -v ArduCopter --console --map
```
Output of this command should contain lines such as:
```
"mavproxy.py" "--master" "tcp:127.0.0.1:5760" "--sitl" "127.0.0.1:5501" "--out" "127.0.0.1:14550" "--out" "127.0.0.1:14551" "--map" "--console"
```
If these commands work without errors, you can proceed to the next steps.

## 2. ROS
You are also going to use [ROS Melodic](https://www.ros.org/about-ros/), which is a popular framework in the robotics community to communicate between processes and machines. Follow the [latest installation instructions](http://wiki.ros.org/melodic/Installation/Ubuntu).

```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl # if you haven't already installed curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-melodic-desktop-full
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
sudo apt install python-rosdep
sudo rosdep init
rosdep update
```

## 3. Gazebo
Gazebo is a 3D physics and robotics simulator with ROS support. A version of Gazebo is available in the ROS repositories you added in step 2, however, it is necessary to use a version of Gazebo from the OSRF repositories.

```bash
sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
wget https://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
sudo apt update
sudo apt install gazebo9
sudo apt install libgazebo9-dev
echo "source /usr/share/gazebo/setup.sh" >> ~/.bashrc
source ~/.bashrc
```

Next, it is necessary to install a gazebo plugin which allows [using Gazebo with Ardupilot SITL](https://ardupilot.org/dev/docs/using-gazebo-simulator-with-sitl.html):
```bash
cd ~
git clone https://github.com/khancyr/ardupilot_gazebo
cd ardupilot_gazebo
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
```

## 4. Mavros
Mavros is a ROS package which can communicate with ardupilot hardware via [Mavlink protocol](https://mavlink.io/en/). This way, you can simulate real-life scenario of controlling a drone with remote process (machine) with ROS.
```bash
cd ~
sudo apt install ros-melodic-mavros ros-melodic-mavros-extras
wget https://raw.githubusercontent.com/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh
chmod a+x install_geographiclib_datasets.sh
sudo ./install_geographiclib_datasets.sh
```

## 5. Setup ROS with ArduPilot SITL
This repository has the structure of a [catkin workspace](http://wiki.ros.org/catkin/workspaces). It contains all the 3D models necessary to simulate an LRS drone in Gazebo. First, clone this repository into your home folder:
```bash
cd ~
git clone https://gitlab.com/fei-lrs/fei_lrs_gazebo.git
```

Prepare ROS workspace:
```bash
cd ~/fei_lrs_gazebo/
catkin init #Output should be: Workspace configuration appears valid.
catkin build

```
To use created ROS workspace: 
- always source ```source ~/fei_lrs_gazebo/devel/setup.bash``` in every terminal where you want to use this ROS workspace
- always source models ```source ~/fei_lrs_gazebo/src/fei_lrs_gazebo/setup.bash``` in every terminal where you want to use this ROS workspace
**OR**

- Add the provided setup files to your `.bashrc` in order to use this workspace in any terminal you open:
```bash
echo "source ~/fei_lrs_gazebo/devel/setup.bash" >> ~/.bashrc
echo "source ~/fei_lrs_gazebo/src/fei_lrs_gazebo/setup.bash" >> ~/.bashrc

```

<!-- ## 5. Setup ROS with ArduPilot SITL
We will create a separate catkin workspace for our first Ardupilot SITL + Airsim project. See [Create a ROS Workspace](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment) for more information.
```bash
cd ~
mkdir -p ardupilot_ws/src
cd ardupilot_ws
catkin_make
cd src
```

Next we will create a ROS package named `lrs_mavros_example`. More details can be found in [this tutorial](http://wiki.ros.org/ROS/Tutorials/CreatingPackage).
```bash
catkin_create_pkg lrs_mavros_example rospy roscpp mavros std_msgs
```

Now you can create a launch file inside the newly created package:
```bash
cd lrs_mavros_example
mkdir launch
cd launch
touch ardu_mav.launch
```
Open the created launch file with text editor (command `gedit ardu_mav.launch` should work) and change content to following:

```xml
<launch>


	<arg name="fcu_url" default="udp://127.0.0.1:14551@14555" />
	<arg name="gcs_url" default="" />
	<arg name="tgt_system" default="1" />
	<arg name="tgt_component" default="1" />
	<arg name="log_output" default="screen" />
	<arg name="fcu_protocol" default="v2.0" />
	<arg name="respawn_mavros" default="false" />

	<include file="$(find mavros)/launch/node.launch">
		<arg name="pluginlists_yaml" value="$(find mavros)/launch/apm_pluginlists.yaml" />
		<arg name="config_yaml" value="$(find mavros)/launch/apm_config.yaml" />

		<arg name="fcu_url" value="$(arg fcu_url)" />
		<arg name="gcs_url" value="$(arg gcs_url)" />
		<arg name="tgt_system" value="$(arg tgt_system)" />
		<arg name="tgt_component" value="$(arg tgt_component)" />
		<arg name="log_output" value="$(arg log_output)" />
		<arg name="fcu_protocol" value="$(arg fcu_protocol)" />
		<arg name="respawn_mavros" value="$(arg respawn_mavros)" />
	</include>
</launch>
```
Check the URL mentioned in line <arg name="fcu_url">, if the line does not mention the same UDP port, change it based on the sim_vehicle.py output.

Now build the created package:
```bash
cd ~/ardupilot_ws
catkin_make
```

Then you need to source the ROS setup files to make it recognize the `~/ardupilot_ws` workspace and the newly-generated `lrs_mavros_example` package. Note that you will need to run this command in every terminal where you want to run ROS commands.
```bash
source ~/ardupilot_ws/devel/setup.bash
```
As an alternative, you can add this line to your `~/.bashrc` file, in which case it will be automatically run in every newly-opened terminal. -->

## 6. Testing the setup ([source1](https://ardupilot.org/dev/docs/using-gazebo-simulator-with-sitl.html), [source2](https://ardupilot.org/dev/docs/ros-sitl.html))

_Hint: when using the default Ubuntu terminal application, you can create new tabs instead of windows using Ctrl+Shift+T._

### Terminal 1: Gazebo visualization
```bash
roslaunch fei_lrs_gazebo fei_lrs_world.launch verbose:=true
```

_Note: If gazebo_ros is not found install: ```sudo apt-get install ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control```_

_Note: Depending on your computer specs, you could also try running the high resolution drone model. It has a more detailed mesh and includes a visual model of the flight computer and other components, but it is mechanically identical to the basic model._
```bash
roslaunch fei_lrs_gazebo fei_lrs_world_hi_res.launch verbose:=true
```
_Note: If gzclient does not find predefined symbols in /usr/lib/x86_64-linux-gnu/libgazebo_common.so.9, upgrade the iginition math library._
```bash
sudo apt upgrade libignition-math2
```
### Terminal 2: Ardupilot SITL
```bash
cd ~/ardupilot/ArduCopter
sim_vehicle.py -f gazebo-iris --console
```
_Note: Optionally you can supply -l argument specifying the starting location as lat,long,alt,heading._
```bash
cd ~/ardupilot/ArduCopter
sim_vehicle.py -f gazebo-iris --console -l 48.15084570555732,17.072729745416016,150,0
```

### Terminal 3: MAVROS
```bash
roslaunch fei_lrs_gazebo fei_lrs_mavros.launch
```

_Note: MAVROS may fail to connect with Ardupilot during its initialization procedure. If that happens, wait for 30-60 seconds and try again._

### Terminal 4 (optional): Display video from drone's stereo camera
The drone is fitted with a stereo camera. The following command is an example of displaying the stream from left camera:
```bash
rosrun image_view image_view image:=fei_lrs_drone/stereo_camera/left/image_raw
```

Alternatively, you can run the `rqt` GUI and use Plugins -> Visualization -> Image View.

### Terminal 5: Testing the simulation

First, you must set drone mode to GUIDED:

```bash
# Note that this command contains a new line
rosservice call /mavros/set_mode "base_mode: 0
custom_mode: 'GUIDED'" 
```

Then you can send an arming command followed by a takeoff command:
```bash
rosservice call /mavros/cmd/arming "value: true"; sleep 1s; rosservice call /mavros/cmd/takeoff "{min_pitch: 0.0, yaw: 0.0, latitude: 0.0, longitude: 0.0, altitude: 1.0}" 
```

In your gazebo visualization, you should see the drone spin up its motors, take off and hover 1 metre above the ground.

The takeoff command may fail during Ardupilot initialization. If that happens, wait and try again. You can also check the Ardupilot Console and wait for a "pre-arm good" message.

If you want to land the drone, send the following command:

```bash
rosservice call /mavros/cmd/land "{min_pitch: 0.0, yaw: 0.0, latitude: 0.0, longitude: 0.0, altitude: 0.0}"
```
