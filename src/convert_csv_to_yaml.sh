# !/bin/bash
cd ..
pwd=$(pwd)
csv_file_name="${pwd}/resources/points_example.csv"

arr_record1=( $(tail -n +1 ${csv_file_name} | cut -d ',' -f1) ) 
arr_record2=( $(tail -n +1 ${csv_file_name} | cut -d ',' -f2) )
arr_record3=( $(tail -n +1 ${csv_file_name} | cut -d ',' -f3) )
arr_record4=( $(tail -n +1 ${csv_file_name} | cut -d ',' -f4) )
arr_record5=( $(tail -n +1 ${csv_file_name} | cut -d ',' -f5) )

for (( i = 0 ; i < ${#arr_record1[@]}-1 ; i++ )) do  arr_record1[$i]=${arr_record1[$i]}","; done
for (( i = 0 ; i < ${#arr_record2[@]}-1 ; i++ )) do  arr_record2[$i]=${arr_record2[$i]}","; done
for (( i = 0 ; i < ${#arr_record3[@]}-1 ; i++ )) do  arr_record3[$i]=${arr_record3[$i]}","; done

for (( i = 0 ; i < ${#arr_record4[@]} ; i++ )) do  arr_record4[$i]="\""${arr_record4[$i]}"\""; done
for (( i = 0 ; i < ${#arr_record4[@]}-1 ; i++ )) do  arr_record4[$i]=${arr_record4[$i]}","; done

for (( i = 0 ; i < ${#arr_record5[@]} ; i++ )) do  arr_record5[$i]="\""${arr_record5[$i]}"\""; done
for (( i = 0 ; i < ${#arr_record5[@]}-1 ; i++ )) do  arr_record5[$i]=${arr_record5[$i]}","; done

cd src/drone_control/config

file_name="config.yaml "

echo "x_coordinates: [${arr_record1[@]}]" >  ${file_name} 
echo "y_coordinates: [${arr_record2[@]}]" >> ${file_name}
echo "z_coordinates: [${arr_record3[@]}]" >> ${file_name}
echo "precision: [${arr_record4[@]}]"     >> ${file_name}
echo "task: [${arr_record5[@]}]"          >> ${file_name}

echo "Coordinates Task executed"


