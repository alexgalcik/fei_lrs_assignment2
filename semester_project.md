# Semester project - simple automatic mission 

### **Project specification**

1. __2D map generation from 3D gazebo LRS world.__
    - right configuration of gazebo plugin (gazebo_ros_2Dmap_plugin) - **required** **DONE**
2. __Map handling.__
    - implementation or tool - map loading - **required**
    - implementation of obstacle inflation - **required**
    - implementation of map transformations, rotations etc. - **if needed**
3. __Map search algorithm. (Path finding)__
    - implementation or tool - path finding/search algorithm - **required**
        - Flood fill, RRT, A*...
4. __Trajectory planning.__
    - implementation or tool - optimization of path - **required**
        - minimal requirement - elimination of not necessary points (elimination of sequences of horizontal, vertical, diagonal paths...)       
    - implementation - heading of the drone in direction of flight (yaw control) - **required**    
5. __ROS Drone control node__
    - implementation - trajectory loading - **required**
    - implementation - mission tasks/commands - **required**
    - implementation - position controller - **required** 
6. __Point specification/task/command__
    - implementation - land and takeoff - **required**
    - implementation - point radius (precision of position controller for given point - Hard/Soft) - **required**
7. __Documentation__ 
    - analysis of each used approach - **required**
        - pros and cons
        - explanation of the algorithms or implementations
    - overal solution diagram - **required**
        - data processing paths
        - ROS Drone control diagram

_Note: Map search algorithm and some parts of Trajectory planning can be implemented in one method, if you choose so._
_Note: If a software tool is used, it must be well documented and explained in documentation._

### **Example task** 


![image](resources/test_map.png)


|| X | Y | Z | Precision| Task |
|---| ---      | ---      | ---      |---  |--- |
|1| x0  | y0   | 2   | - |takeoff |
|2| x1 | y1 | 2 | soft | - |
|3| x2   |   y2    |2   | hard   |land and takeoff |
|4| x3 | y3 | 1| soft| - | 

### **Guides**

__Install simulation environment :__
[ArdupilotSITL_Gazebo_ROS](ardupilot_gazebo_setup.md)

__Basic ardupilot control with MAVROS:__ 
[Basic Mavlink control](ardupilot_sitl_mavros.md)

__2D map from gazebo world:__
[Map generation](map_generator.md)
