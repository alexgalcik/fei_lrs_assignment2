#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"

#include "nav_msgs/MapMetaData.h"

#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/MultiArrayLayout.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Pose.h"
#include <mavros_msgs/State.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/PositionTarget.h>
#include <memory>
#include <chrono>
#include <mutex>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <cmath>
#include <iostream>

#define HARD_DISTANCE 0.4
#define SOFT_DISTANCE 0.25

// #define DEBUG 1

#ifdef DEBUG
// std::vector<double> TEST_DATA = {2.0, 8.600000381469727, 6.899999618530273, 8.65, 2.02,
//                                   3.0, 7.5, 2.0, 7.5, 5.30000114440918, 4.84, 5.37,
//                                   2.0, 2.0, 5.30000114440918, 2.08, 9.74,
//                                   3.0, 2.0, 7.700000762939453, 8.80000114440918, 7.700000762939453, 8.84, 6.9,
//                                   3.0, 7.600000381469727, 6.80000114440918, 7.600000381469727, 8.100000381469727, 2.81, 8.15,
//                                   3.0, 2.8000011444091797, 7.700000762939453, 12.899999618530273, 7.700000762939453, 13.0, 7.0};
std::vector<double> TEST_DATA = {3.0, 8.0, 6.899999618530273, 8.0, 8.899999618530273, 1.85, 9.0, 2.0, 1.8000011444091797, 9.700000762939453, 6.72, 9.71, 3.0, 6.700000762939453, 7.80000114440918, 8.899999618530273, 7.80000114440918, 8.96, 2.0, 3.0, 7.899999618530273, 1.8999996185302734, 7.899999618530273, 5.200000762939453, 2.55, 5.21, 3.0, 2.5, 5.700000762939453, 12.899999618530273, 5.700000762939453, 13.0, 7.0};
const unsigned int TEST_SIZE = 5;
#endif


struct XYZ_pos {
    double x;
    double y;
    double z;
};

struct Obstacle {
    bool enabled;
    bool up;
    bool down;
    bool left;
    bool right;
};

struct Obstacle_treshold {
    int minX;
    int maxX;
    int minY;
    int maxY;
};


ros::Subscriber points_sub, odom_sub, state_sub;
ros::ServiceClient set_mode_client, arming_client, takeoff_client, land_client;
ros::Publisher local_pose_publisher;
ros::Subscriber p2_sub;
mavros_msgs::SetMode guided_set_mode;
mavros_msgs::State current_state;
mavros_msgs::CommandBool arm_cmd;
mavros_msgs::CommandTOL takeoff_request, land_request;
geometry_msgs::Pose pose;
geometry_msgs::Pose tmp_pose;
mavros_msgs::PositionTarget target_position;


std::vector<double> x_coords;
std::vector<double> y_coords;
std::vector<double> z_coords;
std::vector <std::string> precision;
std::vector <std::string> task;

bool points_loaded = false;
bool initial_bool = false;
bool is_obstacle = false;
bool is_rotating = false;
float current_target_yaw = target_position.yaw;
Obstacle obstacle;
Obstacle_treshold forward, left, right, up, down;
float min_z = 1.0, first_z = 0.20, z_distance = 1.0;
float pright = 0.0, pleft = 0.0, pforward = 0.0, pup = 0.0, pdown = 0.0;
std::mutex mutex_;


void getOdometryCallback(const nav_msgs::Odometry::ConstPtr &msg) {
    {
        std::lock_guard <std::mutex> guard(mutex_);
        pose = msg->pose.pose;
        initial_bool = true;
    }
}

void state_cb(const mavros_msgs::State::ConstPtr &msg) {
    current_state = *msg;
}

void setGuidedMode() {
    while (true) {
        {
            std::lock_guard <std::mutex> guard(mutex_);
            tmp_pose = pose;
        }
        if (tmp_pose.position.z > 0.05)
            ros::Duration(5).sleep();
        else
            break;
    }

    std::cout << "Setting guided! " << std::endl;
    guided_set_mode.request.custom_mode = "GUIDED";
    arm_cmd.request.value = true;
    set_mode_client.call(guided_set_mode);
    arming_client.call(arm_cmd);
    ros::Duration(5).sleep();
}

void droneLand(double z) {
    land_request.request.altitude = 0;
    land_client.call(land_request);
    ros::Duration((short) (15 + z)).sleep();
}

void droneTakeOff(double z) {
    setGuidedMode();
    takeoff_request.request.altitude = z;
    takeoff_client.call(takeoff_request);
    ros::Duration((short) (15 + z)).sleep();
}

void droneLandTakeOff(double previous_z, double z) {
    droneLand(previous_z);
    droneTakeOff(z);
    sleep(2);
}

void drone_fly_to_position(XYZ_pos point, bool yaw) {
    target_position.coordinate_frame = 1;
    target_position.position.x = point.x; // meters
    target_position.position.y = point.y; // meters
    target_position.position.z = point.z; // meters

    double angle = atan2((point.y - tmp_pose.position.y), (point.x - tmp_pose.position.x));
    target_position.yaw = angle; //radians

}

float distance_from_point(XYZ_pos target) {
    return sqrt(pow((tmp_pose.position.x - target.x), 2) + pow((tmp_pose.position.y - target.y), 2));
}

bool distance_control(XYZ_pos target, bool is_soft_precision) {
    double safe_dist = sqrt(pow((tmp_pose.position.x - target.x), 2) + pow((tmp_pose.position.y - target.y), 2) +
                            pow((tmp_pose.position.z - target.z), 2));
    if (!is_soft_precision && safe_dist < HARD_DISTANCE) {
        return true;
    } else if (is_soft_precision && safe_dist < SOFT_DISTANCE) {
        return true;
    } else {
        return false;
    }
}

double getCurrentYaw() {
    double quatx = tmp_pose.orientation.x;
    double quaty = tmp_pose.orientation.y;
    double quatz = tmp_pose.orientation.z;
    double quatw = tmp_pose.orientation.w;

    tf::Quaternion q(quatx, quaty, quatz, quatw);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    return yaw;
}

double euclidean_distance(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
}

void PcCallback(const sensor_msgs::PointCloud2ConstPtr &input_pc) {
    sensor_msgs::PointCloud2ConstIterator<float> iterator(*input_pc, "x");
    int width = input_pc->width;
    int height = input_pc->height;
    float data[height][width][3];
    // stred 400x400
    obstacle.enabled = false;
    obstacle.right = false;
    obstacle.left = false;

    pright = 0.0, pleft = 0.0, pforward = 0.0, pup = 0.0, pdown = 0.0;
    float sumright = 0.0, sumleft = 0.0, sumforward = 0.0, sumup = 0.0, sumdown = 0.0;

    if(!is_rotating) {
//    ROS_INFO("d %d, %d", width, height);
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
//                data[i][j][0] = iterator[0]; // X coordinate
//                data[i][j][1] = iterator[1]; // Y coordinate
//                data[i][j][2] = iterator[2]; // Z coordinate
                if (!isnan(iterator[2])) {
                    if (i > forward.minY && i < forward.maxY && j > forward.minX && j < forward.maxX) {
                        if (iterator[2] < min_z) {
                            sumforward += 1;
                        }
                    }
                    if (i > left.minY && i < left.maxY && j > left.minX && j < left.maxX) {
                        if (iterator[2] < min_z) {
                            sumleft += 1;
                        }
                    }
                    if (i > right.minY && i < right.maxY && j > right.minX && j < right.maxX) {
                        if (iterator[2] < min_z) {
                            sumright += 1;
                        }
                    }
                    if (i > up.minY && i < up.maxY && j > up.minX && j < up.maxX) {
                        if (iterator[2] < min_z) {
                            sumup += 1;
                        }
                    }
                    if (i > down.minY && i < down.maxY && j > down.minX && j < down.maxX) {
                        if (iterator[2] < min_z) {
                            sumdown += 1;
                        }
                    }
                }
                ++iterator;
            }

        }


        pforward = sumforward / ((forward.maxX - forward.minX) * (forward.maxY - forward.minY));
        pleft = sumleft / ((left.maxX - left.minX) * (left.maxY - left.minY));
        pright = sumright / ((right.maxX - right.minX) * (right.maxY - right.minY));
        pup = sumup / ((up.maxX - up.minX) * (up.maxY - up.minY));
        pdown = sumdown / ((down.maxX - down.minX) * (down.maxY - down.minY));
        ROS_INFO("X:%0.2f Y:%0.2f FORWAD: %0.2f LEFT: %0.2f RIGHT: %0.2f UP: %0.2f DOWN: %0.2f",tmp_pose.position.x, tmp_pose.position.y, pforward, pleft, pright, pup, pdown);

        if (pforward > 0.0) {
            obstacle.enabled = true;
            if (pleft >= 0.2) {
                obstacle.left = true;
            } else {
                obstacle.left = false;
            }
            if (pright >= 0.2) {
                obstacle.right = true;
            } else {
                obstacle.right = false;
            }
            if (pup >= 0.2) {
                obstacle.up = true;
            } else {
                obstacle.up = false;
            }
            if (pdown >= 0.2) {
                obstacle.down = true;
            } else {
                obstacle.down = false;
            }

        } else {
            obstacle.enabled = false;
        }
        if (obstacle.enabled && obstacle.left && obstacle.right && obstacle.up && obstacle.down) {
            float min = 100;
            if (pup <= min) {
                min = pup;
            }
            if (pdown <= min) {
                min = pdown;
            }
            if (pleft <= min) {
                min = pleft;
            }
            if (pright <= min) {
                min = pright;
            }
            if (min == pup) {
                obstacle.up = false;
            }
            if (min == pdown) {
                obstacle.down = false;
            }
            if (min == pleft) {
                obstacle.left = false;
            }
            if (min == pright) {
                obstacle.right = false;
            }
        }
    }
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "offb_node");
    ros::NodeHandle n;

    //service clients
    p2_sub = n.subscribe("/fei_lrs_drone/stereo_camera/points2", 1, PcCallback);
    state_sub = n.subscribe<mavros_msgs::State>("mavros/state", 10, state_cb);
    arming_client = n.serviceClient<mavros_msgs::CommandBool>("mavros/cmd/arming");
    set_mode_client = n.serviceClient<mavros_msgs::SetMode>("mavros/set_mode");
    takeoff_client = n.serviceClient<mavros_msgs::CommandTOL>("mavros/cmd/takeoff");
    land_client = n.serviceClient<mavros_msgs::CommandTOL>("mavros/cmd/land");
    local_pose_publisher = n.advertise<mavros_msgs::PositionTarget>("mavros/setpoint_raw/local", 10);
    odom_sub = n.subscribe("mavros/global_position/local", 10, getOdometryCallback);
    left.minX = 0;
    left.maxX = 300;
    left.minY = 300;
    left.maxY = 500;

    forward.minX = 300;
    forward.maxX = 500;
    forward.minY = 300;
    forward.maxY = 500;

    right.minX = 500;
    right.maxX = 800;
    right.minY = 300;
    right.maxY = 500;

    up.minX = 300;
    up.maxX = 500;
    up.minY = 0;
    up.maxY = 300;

    down.minX = 300;
    down.maxX = 500;
    down.minY = 500;
    down.maxY = 800;

    ros::Rate loop_rate(20);
    ros::AsyncSpinner spinner(0);
    spinner.start();

    XYZ_pos point = {pose.position.x, pose.position.y, 1.5};
    XYZ_pos target_point = {-8, -2, 0};


    ROS_INFO("Start point x: %0.2f y: %0.2f", point.x, point.y);
    ROS_INFO("Finish point x: %0.2f y: %0.2f", target_point.x, target_point.y);
    droneTakeOff(point.z);
    bool start = true;
    bool start_rotate = true;
    min_z = first_z;

    while (ros::ok()) {
        ros::spinOnce();
        {
            std::lock_guard <std::mutex> guard(mutex_);
            tmp_pose = pose;
        }
        if(start_rotate){
            target_position.yaw = atan2((target_point.y - tmp_pose.position.y), (target_point.x - tmp_pose.position.x)); //radians
            start_rotate = false;
        }
        if (distance_control(target_point, false)) {
            droneLand(0);
        }
        if (distance_control(point, false) || start) {
            if (distance_from_point(target_point) < 0.5) {
                drone_fly_to_position(target_point, start);
            } else if (!is_rotating) {
                if (!obstacle.enabled) {
                    float increment = 0.2;
                    float angle = atan2(target_point.y - tmp_pose.position.y, target_point.x - tmp_pose.position.x);
                    point.x = tmp_pose.position.x + increment * cos(angle);
                    point.y = tmp_pose.position.y + increment * sin(angle);
                    drone_fly_to_position(point, start);
                } else {
                    if (!obstacle.left) {
                        float increment = 0.5;
                        float distance = euclidean_distance(tmp_pose.position.x, tmp_pose.position.y, target_point.x,
                                                            target_point.y);
                        float angle = 90 - atan(increment / distance) * (180.0 / M_PI);
                        point.x = tmp_pose.position.x - increment * cos(angle * M_PI / 180);
                        point.y = tmp_pose.position.y - increment * sin(angle * M_PI / 180);
                    }
                    if (!obstacle.right) {
                        float increment = 0.5;
                        float distance = euclidean_distance(tmp_pose.position.x, tmp_pose.position.y, target_point.x,
                                                            target_point.y);
                        float angle = 90 - atan(increment / distance) * (180.0 / M_PI);
                        point.x = tmp_pose.position.x + increment * cos(angle * M_PI / 180);;
                        point.y = tmp_pose.position.y + increment * sin(angle * M_PI / 180);

                    }
                    if (!obstacle.down) {
                        point.z -= 0.5;
                    }
                    if (!obstacle.up) {
                        point.z += 0.5;
                    }
                    drone_fly_to_position(point, start);
                }
            }
            min_z = z_distance;
            start = false;

        }
        if (target_position.yaw != current_target_yaw) { // prisla zmena zelaneho otocenia
            current_target_yaw = target_position.yaw;
            is_rotating = true;
            local_pose_publisher.publish(target_position);
        } else if (is_rotating && abs(current_target_yaw - getCurrentYaw()) < 0.1) {
            is_rotating = false;
        }

        loop_rate.sleep();
    }

    return 0;
}
