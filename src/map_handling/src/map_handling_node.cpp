#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>


#include "nav_msgs/OccupancyGrid.h"
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"

#include <iostream>
#include <fstream>


#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#define N_OF_MAPS 3 // Pocet urovni mapy
#define HALF_SAFE_HEIGHT 0.1 // 1/2 hodnoty vysky cez ktoru dron dokaze preletiet
#define RADIUS_DRONE 0.28
// #define RADIUS_DRONE 0.6


#include <gazebo_ros_2Dmap_plugin/GenerateMap.h>

const char *homedir;
unsigned short MAX_POINTS;
int number_of_points = 0;
int number_of_maps = 0;
ros::Subscriber map_sub;
nav_msgs::OccupancyGrid* occupancy_map;
short *temp_map_vector;
short *all_maps_vector;
unsigned int idx_maps_vec = 0;
bool reading_map = false;
short drone_home_x = 13;
short drone_home_y = 7;
short map_offset_x = -18;
short map_offset_y = -18;

void getMapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  std_msgs::Header header = msg->header;
  nav_msgs::MapMetaData info = msg->info;

  if(number_of_maps == 0){
		temp_map_vector = (short *)calloc(info.width * info.height, sizeof(short));
	}
  if(number_of_points == 0){
		all_maps_vector = (short *)calloc(1 + MAX_POINTS * info.width * info.height, sizeof(short));
	}

  ROS_INFO("Got map %d %d", info.width, info.height);
  // std::ofstream map_file;
  // map_file.open(homedir, std::ios::app);
  // if (map_file.is_open()) {
  //   map_file << "width: ," << info.width << ", \n";
  //   map_file << "height: ," << info.height << ", \n";

  //   for (unsigned int x = 0; x < info.width; x++) {
  //     for (unsigned int y = 0; y < info.height; y++) {
  //         map_file << (int)msg->data[x + info.width * y] << ", ";
  //     }
  //     map_file << "\n";
  //   }
  //   ROS_INFO("Map written to file");
  //   map_file.close();
    
  // }

  for(int i = 0; i < info.width * info.height; i++)
  {
    if(msg->data.at(i)){
      *(temp_map_vector + i) = 1; // TU SA POSTUPNE ZLUCIA TRI MAPY DO JEDNEJ
    }
  }

  if(++number_of_maps == (N_OF_MAPS)){

    /*
    TU TREBA SPRAVIT FUNKCIU CO SPOJI MAPY A POTOM ZACNE POSIELAT DATA NA TOPIC
    */
    number_of_maps = 0;

    short map[info.width][info.height];
    short new_map[info.width][info.height];
    unsigned short size_of_infl = RADIUS_DRONE/info.resolution + 2;
    for (unsigned int x = 0; x < info.width; x++) {
      for (unsigned int y = 0; y < info.height; y++) {
        map[x][y] = *(temp_map_vector + (x + info.width*y));  // ZO ZLUCENEJ MAPY SPRAVIM 2D POLE A NASLEDUJE INFLACIA
        short xdrone = (drone_home_x/info.resolution)-(map_offset_x/info.resolution)+1;
        short ydrone = (drone_home_y/info.resolution)-(map_offset_y/info.resolution)+1;
        if(x == xdrone && y == ydrone){
          for(int xx = -size_of_infl; xx <= size_of_infl; xx++){
            for(int yy = -size_of_infl; yy <= size_of_infl; yy++){
              map[x+xx][y+yy] = 0;
            }
          }
        }
        new_map[x][y] = map[x][y];
      }
    }
    unsigned short counter = 1;
    
    for(int i = 0; i < info.width; i++){
      for(int j = 0; j < info.height; j++){
        // new_map[i][j] = map[i][j];

        if(map[i][j] == 1){
          for(int ii = -size_of_infl; ii <= size_of_infl; ii++){
            for(int jj = -size_of_infl; jj <= size_of_infl; jj++){
              if((i+ii >= 0 && i+ii < info.width) && (j+jj >= 0 && j+jj < info.height)){ // pre istotu ci nieje nahodou mimo mapy(pola)
                new_map[i+ii][j+jj] = 1;            //INFLACIA PREKAZOK MAPY
              }
            }
          }
        }
      }
    }

    for(int i = 0; i < info.width; i++){
      for(int j = 0; j < info.height; j++){
        all_maps_vector[(number_of_points * info.width * info.height) + (i + info.width * j)] = new_map[i][j];
        idx_maps_vec++;
      }
    }

    if(++number_of_points == (MAX_POINTS)){
      map_sub.shutdown();
      occupancy_map->data.resize(1 + MAX_POINTS * info.width * info.height);
      occupancy_map->header.stamp = ros::Time::now();
      occupancy_map->header.frame_id = "map"; //TODO map frame
      occupancy_map->info.map_load_time = ros::Time(0);
      occupancy_map->info.resolution = info.resolution;
      occupancy_map->info.width = info.width;
      occupancy_map->info.height = info.height;
      occupancy_map->info.origin.orientation.w = 1;
      occupancy_map->info.origin.position = msg->info.origin.position;
      occupancy_map->data.at(0) = MAX_POINTS;
      //v tomto fore naplnit mapu kt chceme publikovat
      for (unsigned int i = 0; i < idx_maps_vec; i++){
        occupancy_map->data.at(i+1) = *(all_maps_vector + i);
      }

    }
  }
  reading_map = false;
}

int main(int argc, char **argv)
{

  if ((homedir = getenv("HOME")) == NULL) {
      homedir = getpwuid(getuid())->pw_dir;
  }
  char *path_from_home = (char *)"/fei_lrs_gazebo/map.txt"; 
  char *tmp = new char[strlen(homedir) + strlen(path_from_home) + 1];

  strcpy(tmp, homedir);
  strcat(tmp, path_from_home);
  homedir = tmp;

  std::ofstream map_file(homedir);
  map_file.close();
  occupancy_map = new nav_msgs::OccupancyGrid();


  ros::init(argc, argv, "map_handler");


  ros::NodeHandle n;

  ros::Publisher map_publisher = n.advertise<nav_msgs::OccupancyGrid>("map_with_inflation", 10);
  ros::ServiceClient client = n.serviceClient<gazebo_ros_2Dmap_plugin::GenerateMap>("gazebo_2Dmap_plugin/generate_map");
  
  ros::Rate loop_rate(10);

  gazebo_ros_2Dmap_plugin::GenerateMap srv;
  std::vector<double> heights;
  n.getParam("/z_coordinates", heights);
  MAX_POINTS = heights.size();

  std::cout << "\nVector elements are: ";
  for (auto it = heights.begin(); it != heights.end(); it++)
    std::cout << *it << " ";
  std::cout << std::endl;

  map_sub = n.subscribe("map2d",10,getMapCallback);

  while (ros::ok())
  {
    if (reading_map == false && number_of_points < MAX_POINTS) {
      reading_map = true;
      std::cout << "number_of_maps: " << number_of_maps << std::endl;
      srv.request.height = (heights.at(number_of_points) - (double)HALF_SAFE_HEIGHT) + (double)HALF_SAFE_HEIGHT * number_of_maps;
      std::cout << "height: " << srv.request.height << std::endl;
      if (client.call(srv)) {
        if(srv.response.done){
          ROS_INFO("service response: TRUE");
        } else {
          ROS_INFO("service response: FALSE");
        }
      }
    }
    if (number_of_points >= (MAX_POINTS)) {
      map_publisher.publish(*occupancy_map);
    }

    ros::spinOnce();

    loop_rate.sleep();   
  }
  return 0;
}
