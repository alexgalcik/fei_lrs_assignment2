# Spustenie zakladnych veci pre simulaciu: #

## Gazebo visualization
```bash
roslaunch fei_lrs_gazebo fei_lrs_world.launch verbose:=true
```
## Ardupilot SITL
```bash
cd ~/ardupilot/ArduCopter
sim_vehicle.py -f gazebo-iris --console
```
## MAVROS
```bash
roslaunch fei_lrs_gazebo fei_lrs_mavros.launch
```

_Note: MAVROS may fail to connect with Ardupilot during its initialization procedure. If that happens, wait for 30-60 seconds and try again._


# Spustenie jednotlivych node:  #

## map_handling:  ##

```
roslaunch map_handling map_handling.launch
```

## path_planning:  ##

```
roslaunch path_planning path_planning.launch
```
## drone_control:  ##
```
roslaunch drone_control drone_control.launch
```
## parametre ktore nastavime na parametrovy server:  ##
Nastavenie parametrov sa nachádza v nasledovnom subore: drone_control/config/config.yaml

```
x_coordinates: [13.00, 8.65, 4.84, 2.08, 8.84, 2.81, 13.00]
y_coordinates: [7.00, 2.02, 5.37, 9.74, 6.90, 8.15, 7.00]
z_coordinates: [2.00, 1.39, 0.42, 1.46, 2.00, 2.27, 2.00]
precision: ["soft", "soft", "hard", "hard", "hard", "soft", "hard"]
task: ["takeoff", "-", "-", "-", "landtakeoff", "-", "land"]
```