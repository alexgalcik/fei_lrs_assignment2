#!/bin/bash

gnome-terminal\
    --tab\
        --title="TAB Gazebo" -- bash -c "cd ~; roslaunch fei_lrs_gazebo fei_lrs_world.launch verbose:=true; $SHELL"
sleep 10;
gnome-terminal\
    --tab\
        --title="TAB Ardupilot" -- bash -c "cd ~/ardupilot/ArduCopter; sim_vehicle.py -f gazebo-iris --console; $SHELL"\
sleep 2
gnome-terminal\
    --tab\
        --title="TAB Mavros" -- bash -c "cd ~; sleep 35; roslaunch fei_lrs_gazebo fei_lrs_mavros.launch; $SHELL"\

gnome-terminal\
    --tab\
        --title="TAB TestDrone" -- bash -c "cd ~; sleep 37; roslaunch drone_control test_drone_control.launch; $SHELL"\
