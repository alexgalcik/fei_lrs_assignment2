# Lietajúce robotické systémy

Dokumentácia k semestrálnej práci - riadenie drona

------------------

## Štruktúra projektu

Náš projekt pozostáva z nasledujúcich uzlov (nodes):

 - 2 externé uzly - FEI_LRS_GAZEBO a GAZEBO_ROS_2DMAP_PLUGIN
 - 3 vlastné naprogramované uzly - MAP_HANDLING, PATH_PLANNING, DRONE_CONTROL

Každý uzol zodpovedá za časť úlohy riadenia drona, kedy príkazy do MAVROSU posiela riadiací uzol DRONE_CONTROL. Na obrázkoch nižšie je znázornená škturkúra prepojenia jednotlivých uzlov.

![](/assets/struktura.png)<br />

V nasledujúcej časti si prejdeme jednotlivé uzly.

# Dokumentácia uzlov

## FEI_LRS_GAZEBO
Je to uzol, ktorý má na starosti spustenie Gazebo simulátora s celým vytvoreným simulačným svetom. Bol nám poskytnutý na GitLab repozitáry predmetu a je to východiskový bod nášho projektu. Tento uzol teda nebudeme dôkladne popisovať.


## GAZEBO_ROS_2DMAP_PLUGIN
Tento balíček je externý a je verejne prístupný na internrete. Pomocou neho vieme vygenerovať 2D mapu s požadovanými parametrami z Gazebo sveta. 
Tento balíček sme však upravili tak, aby vyhovoval našim potrebám. Konkrétne sme v ros service `gazebo_2Dmap_plugin/generate_map` urobili takú zmenu, aby príjimala v requeste výšku, v ktorej má generovať mapu. To nám poslúži uskutočniť našu zamýšlanú logiku hľadania voľnej trajektórie k žiadanému bodu. Tá je opísaná v nasledujúcom bode.

## MAP_HANDLING
V tomto uzle generujeme mapu prostredia pomocou ros service `gazebo_2Dmap_plugin/generate_map` definovanou v balíčku gazebo_ros_2Dmap_plugin. Túto service voláme pre každý žiadaný bod v 3 rôznych výškach, nakoľko náš dron má 3 rozmery (aj výšku) a teda potrebujeme pokryť letové pásmo vo všetkých osiach (aj os z).

Pre každý bod našej misie teda vytvoríme na začiatku 3 mapy, kedy:
 - prvá mapa je  vo výške bodu -0.1 cm
 - druhá mapa je vo výške bodu
 - tretia mapa je vo výške bodu + 0.1 cm

Dokopy teda budeme mať `počet máp = 3 x počet bodov`.

Následne tieto 3 mapy zlúčime, tak že výsledná mapa je zjednotením všetkých prekážok daných máp. Tieto prekážky na každej mape rozšírime o polomer robota, tak aby to zodpovedalo jeho reálnej konštrukcii, ktorá má určitú šírku. Výška je zohľadnená práve 3 mapami v 3 výškach daného bodu.

Následne takto vygenerované mapy publikujeme na tému `/map_with_inflation`.

## PATH_PLANNING
V path_planning uzle sme implementovali algoritmus plánovania trasy. Generujú sa tu body, cez ktoré dron bude musieť prejsť, aby sa dostal do cieľa s tým, že obíde prekážky bez kolízie. Plánujeme teda trasu medzi každými dvomi susednými bodmi definovanej trajektórie, pričom obchádzame prekážky zodpovedajúce danej výške.

Na mapách získaných z map_handling uzla používame záplavový algoritmus plánovania trajektórie. Následne musí byť získaná trajektória ešte zjednodušená tak, že sa z nej extrahujú iba body, pri ktorých sa mení jej smer. Tieto prechodové body následne pridávame do existujúcej trajektórie.

Takto zostrojenú sekvenciu bodov publikujeme na tému (topic) `/trajectory_points`. Túto sekvenciu posielame nami vytovreným 'protokolom'. Ten je reprezentovaný správou ako 1D pole, pričom toto pole sa skladá z:
- Pre každý bod misie máme správu dlhú N reálnych čísiel.
- Prvé číslo hovorí, koľko dvojíc XY bodov máme na ceste k   cielovému bodu. T.j., ak k cielu potrebujeme ísť cez pomocné 2 body, číslo bude mať hodnotu 3 (zahŕňame aj ciel).
- Nasledujú dvojice XY bodov vrátane cielového bodu
- Tento rámec sa opakuje toľko krát, koľko cielových bodov máme

Pr. [K_0, X_0, Y_0, X_1, Y_1, X_2, Y_2, K_1, X_3, Y_3, X_4, Y_4]

## DRONE_CONTROL
Tento uzol vykonáva celý riadiaci algoritmus robota. Toto zabezpečíme pomocou .launch súboru, ktorý najskôr načíta parametre zo súboru `config.yaml` na parametrový server. Následne postupne v sekvencii zavolá všetky .launch súbory všetkých potrebných predchádzajúcich balíčkov. Následne spustí uzol `drone_control`. V zásade teda spustíme zvlášť Gazebo svet a zvlášť náš ROS projekt pomocou `drone_control.launch`.
Balíček `path_planning` čaká na správu z `map_handling`, a teda takto je zabezpečená synchronizácia medzi jednotlivými balíčkami. Ďalej,
vstupom (alebo prerekvizitou) sú body v danom formáte z predchádzajúceho uzla. Na tieto taktiež čakáme, dokedy nie sú odoslané z path_planning balíčka. Po úspešnom prijatí bodov začneme s logikou riadenia misie. \
Pre real-time dáta z odometrie sme v tomto uzle použili asynchrónny spinner (vďaka tomuto vieme obsluhovať subscriber callback `getOdometryCallback` s vysokou frekvenciou - ten nám vracia aktuálne súradnice drona). 
Našu riadiacu štuktúru môžeme volať s nižšou frekvenciou a teda tým zabezpečíme to, že v každom riadiacom cykle budeme mať aktuálne dáta z robota a budeme presne vediet určiť, či už sme v blízkosti cielového boda, alebo nie.\
Ak sme v blízkosti cielového bodu, náš algoritmus automaticky prepne riadenie k ďaľšiemu bodu zo zásobníka, ktorý sme si pred riadiacim cyklom naplnili. 

<br/>


# Spustenie projektu

Na spustenie projektu pomocou skriptu je potrebné mať nainštalovaný terminator terminál.
```bash
sudo apt install terminator
```

Na zjednodušenie spustenia celého projektu sme vytvorili skript, ktorý automaticky spustí Gazebo Simulátor a virtuálny model drona ArduCopter. Automatické spustenie Mavrosu a nášho ROS riadiaceho programu sme nedokázali zabezpečiť, nakoľlko Mavros môžeme spustiť až po inicializácii ArduCopter modelu, avšak táto inicializácia trvá vždy rôzny čas. A samozrejme, náš ROS projekt môžeme spustiť až po spustení Mavrosu.

Pre čo najjedoduchšie obslúženie spustenia celej aplikácie sme teda vytvorili nasledovný 'flow':

```bash
cd ~   # (podmienka fungovania startup skriptu je klonovanie do home priečinku)

git clone https://gitlab.com/jaroslav.balko/fei_lrs_gazebo.git

cd ~/fei_lrs_gazebo/src

chmod 777 launchscript.sh
chmod 777 csvpoints.sh

./launchscript.sh
```

Následne sa otvoria 4 okná terminator, kedy v 1. z nich sa spúšťa Gazebo a v 2. z nich sa spúšťa ArduCopter.\
Tretie okno nám poslúži na spustenie Mavrosu, ktorý však musíme spustiť manuálne, a preto v tomto terminály iba pomocou príkazu echo vypíšeme, ako spúšťame Mavros (keď už Mavros nemôžeme spustiť automaticky, aspoň si vypíšeme príkaz, ktorý musíme spustiť). Rovnaké je to so 4. oknom, avšak toto bude slúžiť na manuálne spustenie nášho ROS projektu. Na spustenie všetkých uzlov v správnom poradi sme vytvorili `.launch` súbor, ktorý sa nachádza v poslednom uzle nášho projektu `drone_control`. Rovnako si do tohto terminálu vypíšeme cez echo, ako tento sumárny `.launch` súbor spustiť. 

Po spustení uzla `drone_control` sa začne vykonávať misia špecifikovaná v `config.yaml` v `/src/drone_control/config/`.


Na vygenerovanie nového `config.yaml` súboru, ktorý bude obsahovať nové letové súradnice misie pre otestovanie funkčnosti projektu, sme vytvorili bash script, ktorý prekonvertuje zadaný `.csv` súbor na súbor typu `.yaml`. 
Jeho spustenie prebieha rovnako automaticky pri spustení `./launchscript.sh`. K jeho spusteniu je potrebné nasledovné:

- Požadovaný súbor `.csv` (s novými súradnicami pre testovaciu misiu) bol taktiež v rovnakom priečinku ako `csvpoints.sh` script.
Nový `.csv` súbor premenovať na `points.csv` alebo otovrit `csvpoints.sh` script a prepisať meno čítaného súboru na požadované meno.

# Kto/Čo spravil
Tím 2 - Štvrtok 10:00; Získané body: 267
 - Jaroslav Balko -> Štruktúra projektu, implementácia celej distribúcie dát medzi balíčkami (45 bodov)
 - Lukáš Čierny -> implementácia map_handling balíčka, práca na drone_control balíčku (45 bodov)
 - Ladislav Brečka -> implementácia spracovania dát z path_planning do drone_control (45 bodov)
 - Alex Galčík -> implementácia funkcionality uloh drona (vzlet, pristávanie, ...) (45 bodov)
 - Miroslav Đemrovski -> fixnutie bugov na dron_control_node (yaw natocenie na mieste a oprava LandTakeOff úlohy) (44 bodov)
 - Matúš Dankanin -> pomoc pri implementovani dron_control_node a vytvorenie bash scriptov (43 bodov)


